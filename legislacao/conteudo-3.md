
Aos que se lançam no estudo das regiões metropolitanas brasileiras, o ano de 1973 tem especial significância, haja vista que, no referido ano, são criadas as primeiras regiões metropolitanas brasileiras, por força da Lei Complementar n° 14 /1973. Assim, foram institucionalizadas inicialmente, nove regiões metropolitanas: as de São Paulo, Belo Horizonte, Porto Alegre, Curitiba, Salvador, Recife, Fortaleza, Belém e Rio de Janeiro, esta última em 1974.



