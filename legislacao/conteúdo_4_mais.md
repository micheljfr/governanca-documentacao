# Acesse as leis complementares por unidades da federação 
[**Alagoas**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Alagoas.zip)

[**Amapa**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Amap%C3%A1.zip)

[**Amazonas**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Amazonas.zip) 

[**Bahia**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Bahia.zip)

[**Ceara**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Cear%C3%A1.zip) 

[**Distrito Federal**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/DF.zip)

[**Espirito Santo**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Espirito%20Santo.zip) 

[**Goias**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Goias.zip)

[**Maranhao**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Maranh%C3%A3o.zip) 

[**Mato Grosso**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Mato%20Grosso.zip)

[**Minas Gerais**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Minas%20Gerais.zip) 

[**Paraiba**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Paraiba.zip)

[**Parana**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Paran%C3%A1.zip)

[**Para**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Par%C3%A1.zip) 

[**Pernambuco**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Pernambuco.zip)

[**Piaui**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Piau%C3%AD.zip) 

[**Rides**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/RIDES.zip)

[**Rio Grande do Norte**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Rio%20Grande%20do%20Norte.zip)

[**Rio Grande do Sul**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Rio%20Grande%20do%20Sul.zip) 

[**Rio de Janeiro**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Rio%20de%20Janeiro.zip)

[**Rondonia**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Rondonia.zip) 

[**Roraima**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Roraima.zip)

[**Santa Catarina**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Santa%20Catarina.zip) 

[**Sergipe**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Sergipe.zip)

[**Sao Paulo**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/S%C3%A3o%20Paulo.zip) 

[**Tocantis**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Tocantins.zip)

[**Todas Lcs Estaduais**](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/legislacao/leis-complementares/Todas%20LCs%20Estaduais.zip)


