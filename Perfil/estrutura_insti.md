# RM Belém
## cod=1501 

![belen](imgs/est-inst-belem.png?raw=true)

### id=codem

#### coords=294,65,591,146

### id=seplan

#### coords=38,235,335,318

### id=fdn

#### coords=541,235,838,318  

# RM Belo Horizonte
## cod=3101

![bh](imgs/est-inst-belo-horizonte.png?raw=true)

### id=conselho

#### coords=296,73,444,131

### id=assembleia

#### coords=694,73,844,131

### id=agem

#### coords=39,224,282,298 

### id=fdm

#### coords=457,224,700,298

# RM Curitiba
## cod=4101

![curitiba](imgs/est-ins-solução curitiba.png?raw=true)

### id=comec

#### coords=38,90,261,392

### id=municipal

#### coords=38,554,336,635

### id=assomec

#### coords=522,652,843,780 

# RIDE DF
## cod=53

![df](imgs/est-inst-df.png?raw=true)

### id=coaride

#### coords=269,48,566,106 

### id=secretaria

#### coords=269,157,566,198

# RM Florianópolis 
## cod=4201

![florianopolis](imgs/est-inst-florianopolis.png?raw=true)

### id=suderf

#### coords=402,55,700,148

### id=coderf

#### coords=175,226,471,299

### id=colegio

#### coords=692,226,840,282

### id=superintendencia

#### coords=29,371,273,429

### id=diretoria

#### coords=355,371,628,429

# RM Fortaleza
## cod=2301

![fortaleza](imgs/est-inst-fortaleza.png?raw=true)

### id=cdm

#### coords=293,68,589,126 

### id=fdm

#### coords=293,182,589,240

# RM Goiânia
## cod=5201

![goiania](imgs/est-inst-goiania.png?raw=true)

### id=conselho

#### coords=297,46,594,105

### id=secretaria

#### coords=39,139,335,180

### id=cdtc

#### coords=135,271,214,319

### id=camara

#### coords=341,271,553,349

### id=demais

#### coords=611,271,823,349

### id=cmtc

#### coords=135,340,214,388

# RM São Luís
## cod=2101

![sao luis](imgs/est-inst-sao-luiz.png?raw=true)

### id=coadegs

#### coords=293,68,589,126

# RM da Grande Vitória
## cod=3201

![vitoria](imgs/est-inst-vitoria.png?raw=true)

### id=comdevit

#### coords=294,29,591,110

### id=cagem

#### coords=68,204,310,286

### id=ijsn

#### coords=568,204,810,286

### id=cates

#### coords=38,364,225,446

### id=ge

#### coords=261,366,451,446

### id=fumdevit

#### coords=512,364,810,446

# RM Porto Alegre
## cod=4301

![porto alegre](imgs/est-ins-solução porto alegre.png?raw=true)

### id=diretoria

#### coords=429,209,626,326

### id=representativas

#### coords=666,157,844,234

### id=gestao

#### coords=279,346,554,439

# RM Recife
## cod=2601

![recife](imgs/est-inst-recife.png?raw=true)

### id=conderm

#### coords=222,26,518,121

### id=conselho

#### coords=546,26,843,121

### id=funderm

#### coords=38,138,335,196

### id=camaras

#### coords=580,138,843,185

### id=funderm2

#### coords=38,214,335,272

### id=cmds

#### coords=221,298,300,347

### id=cmt

#### coords=357,298,436,347

### id=cmduot

#### coords=493,298,572,347

### id=cmmas

#### coords=629,298,708,347

### id=cmpds

#### coords=765,298,844,347

# RM Rio de Janeiro
## cod=3301

![Rj](imgs/est-inst-rio-de-janeiro.png?raw=true)

### id=gestor

#### coords=282,96,430,154

### id=consultivo

#### coords=681,96,830,154

### id=gt

#### coords=58,246,236,307 

### id=executivo

#### coords=443,246,687,320

# RM Salvador
## cod=2901

![salvador](imgs/est-inst-salvador.png?raw=true)

### id=colegiado

#### coords=221,33,519,115

### id=secretario

#### coords=221,138,519,197

### id=tecnico

#### coords=38,211,334,328

### id=conselho

#### coords=402,211,699,328

### id=tematico

#### coords=38,355,334,455

### id=grupos

#### coords=402,355,699,455

# RM São Paulo
## cod=3501

![sp](imgs/est-inst-sao-paulo.png?raw=true)

### id=cdrmsp

#### coords=297,68,594,143

### id=comite

#### coords=117,227,266,284

### id=comissao

#### coords=618,227,767,284 

### id=grupos

#### coords=117,327,266,384

### id=emplasa

#### coords=618,327,767,384

# RM Vale do Rio Cuiabá
## cod=5101

![cuiba](imgs/est-inst-cuiaba.png?raw=true)

### id=codem

#### coords=296,67,593,141

### id=agem

#### coords=41,227,338,301

### id=fds

#### coords=544,227,839,301 
