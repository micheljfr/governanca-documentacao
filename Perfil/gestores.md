# RM Belém
## cod=1501

### Helena Tourinho - Diretora de Desenvolvimento Metropolitano na Secretária de Desenvolvimento Urbano e Obras Públicas do Estado do Pará

# RM Belo Horizonte
## cod=3101

### Flávia Mourão Parreira do Amaral - Diretora Geral Agência RMBH

# RM Curitiba
## cod=4101

### Carlos Do Rego Almeida Filho - Coordenador Geral da Região Metropolitana de Curitiba 

# RIDE DF
## cod=53

### Antônio Carlos Nantes de Oliveira - Superintendente da SUDECO

# RM Florianópolis 
## cod=4201

### Cássio Taniguchi - Superintendente da SUDERF

# RM Fortaleza
## cod=2301

### George Valentim - Presidente do Inesp

# RM Goiânia
## cod=5201

### Marcelo Safadi - Superintendente de Assuntos Metropolitanos

# RM São Luís
## cod=2101

# RM da Grande Vitória
## cod=3201

### Gabriela Gomes de Macêdo Lacerda - Diretora Presidente do IJSN

# RM Porto Alegre
## cod=4301

### Pedro Bisch Neto - Diretor Superintendente da Metroplan

# RM Recife
## cod=2601

### Bruno de Moraes Lisboa - Presidente CONDEPE/FIDEM

# RM Rio de Janeiro
## cod=3301

### Vicente Loureiro - Diretor Executivo da Câmara Metropolitana 

# RM Salvador
## cod=2901

### Fernando Torres - Secretário de Desenvolvimento Urbano no Estado da Bahia

# RM São Paulo
## cod=3501

### João Agripino da Costa Doria Junior - Prefeito do Município de São Paulo

# RM Vale do Rio Cuiabá
## cod=5101

### Maristene Amaral Matos - Presidente da Agência Metropolitana do Vale do Rio Cuiabá Agem/VRC