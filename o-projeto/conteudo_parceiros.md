# Parceiros

[![](imgs/proj_parce_BH.PNG?raw=true)](http://www.agenciarmbh.mg.gov.br/)
## Agência de Desenvolvimento da RMBH

[![](imgs/proj_parce_DF.PNG?raw=true)](http://www.codeplan.df.gov.br/)
## Companhia de Planejamento do Distrito Federal - CODEPLAN

[![](imgs/proj_parce_SP.PNG?raw=true)](https://www.emplasa.sp.gov.br/)
## Empresa Paulista de Planejamento Metropolitano S.A

[![](imgs/proj_parce_RJ.PNG?raw=true)](http://www.camarametropolitana.rj.gov.br/)
## Câmara Metropolitana do Rio de Janeiro 

[![](imgs/proj_parce_MT.PNG?raw=true)](http://www.fundacaouniselva.org.br/novoSite/)
## Fundação de Apoio e Desenvolvimento da UFMT (Fundação Uniselva)

[![](imgs/proj_parce_FEE.PNG?raw=true)](https://www.fee.rs.gov.br/)
## Fundação de Economia e Estatística Siefried Emanuel Heuser - FEE

[![](imgs/proj_parce_FJN.png?raw=true)](http://www.fundaj.gov.br/)
## Fundação Joaquim Nabuco - FUNDAJ

[![](imgs/proj_parce_PA.PNG?raw=true)](http://www.idesp.pa.gov.br/)
## Instituto de Desenvolvimento Econômico, Social e Ambiental do Pará - IDESP

[![](imgs/proj_parce_CE.PNG?raw=true)](http://www.ipece.ce.gov.br/) 
## Instituto de Pesquisa e Estratégia Econômica do Ceará - IPECE

[![](imgs/proj_parce_IJSN.png?raw=true)](http://www.ijsn.es.gov.br/)
## Instituto Jones dos Santos Neves - IJSN

[![](imgs/proj_parce_PR.png?raw=true)](http://www.ipardes.gov.br/)
## Instituto Paranaense de Desenvolvimento Econômico e Social - IPARDES

[![](imgs/proj_parce_MA.png?raw=true)](http://imesc.ma.gov.br/portal/Home)
## Instituto Maranhense de Estudos Socioconômicos e Cartográficos - IMESC

[![](imgs/proj_parce_SECIMA.PNG?raw=true)](http://pdi-rmg.secima.go.gov.br/) 
## Instituto de Desenvolvimento da Região Metropolitana de Goiânia - SDR GO

[![](imgs/proj_parce_Am-se.PNG?raw=true)](http://www.seplancti.am.gov.br/)
## Secretária de Estado de Planejamento e Desenvolvimendo Econômico

[![](imgs/proj_parce_SC.PNG?raw=true)](http://www.spg.sc.gov.br/suderf)
## Superintendência de Desenvolvimento da Região Metropolitana da Grande Florianópolis – SUDERF

[![](imgs/proj_parce_metropLan.PNG?raw=true)](http://www.metroplan.rs.gov.br/)
## Fundação Estadual de Planejamento Metropolitano e Regional do Estado do Rio Grande do Sul 

[![](imgs/proj_parce_bahia.PNG?raw=true)](http://www.sedur.ba.gov.br/)
## Secretária de Desenvolvimento Urbano do Estado da Bahia 

[![](imgs/proj_parce_METRODS.png?raw=true)](http://casafluminense.org.br/projetos/caderno-de-experiencias-ods-no-rio-metropolitano-e-brasil/)
## Observatório Metropolitano - Casa Fluminense 

[![](imgs/proj_parce_Natal.PNG?raw=true)](http://www.ufrn.br/)
## Universidade Federal do Rio Grande do Norte

[![](imgs/proj_parce_BA.png?raw=true)](http://www.sei.ba.gov.br/)
## SEI – Superintendência de Estudos Econômicos e Sociais da Bahia 

[![](imgs/proj_parce_CA_Logo.png?raw=true)](http://www.citiesalliance.org/)
## Cities Alliance 

[![](imgs/proj_parce_Condepe.png?raw=true)](http://www.condepefidem.pe.gov.br/web/condepe-fidem)
## Agência Estadual de Planejamento e Pesquisas de Pernambuco
