

**Volume 1** - [40 anos de Regiões Metropolitanas no Brasil] (http://www.ipea.gov.br/portal/index.php?option=com_content&view=article&id=19647)

**Volume 2** - [Funções Públicas de Interesse Comum nas Metrópoles Brasileiras: transportes, saneamento básico e uso do solo] (http://www.ipea.gov.br/portal/index.php?option=com_content&view=article&id=24031)

**Volume 3** - [Relatos e Estudos de Caso da Gestão Metropolitana no Brasil] (http://www.ipea.gov.br/portal/index.php?option=com_content&view=article&id=24032)




 
 